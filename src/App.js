import React, {useEffect, useState} from 'react'
import axios from 'axios'
import TaskList from "./TaskList";


function App() {
  const [tasks, setTasks] = useState([])
  const [taskTitle, setTaskTitle] = useState('')
  const [filter, setFilter] = useState('any')

  useEffect(async () => {
    try {
      await axios('https://jsonplaceholder.typicode.com/todos')
        .then(response => setTasks(response.data))
        // .then(json => setTasks(json))
    } catch (e) {
      alert('something went wrong: ' + e)
    }
  },[]);


  // useEffect(() => {
  //   async function jsonTask() {
  //     try {
  //       fetch('https://jsonplaceholder.typicode.com/todos/1')
  //         .then(response => response.json())
  //         .then(json => setTasks([json]))
  //     } catch (e) {
  //       alert('something went wrong: ' + e)
  //     }
  //   }
  //
  //   Promise.all(jsonTask())
  // }, [])


  // useEffect(() => {
  //   const todos = localStorage.getItem('tasks')
  //   setTasks(JSON.parse(todos))
  // }, [])
  // useEffect(() => {
  //   localStorage.setItem('tasks', JSON.stringify(tasks))
  // }, [tasks])

  // useEffect(() => {
  //   const todosRaw = localStorage.getItem('tasks')
  //
  //   try {
  //     const todos = JSON.parse(todosRaw)
  //
  //     if (Array.isArray(todos) && todos.length > 0) {
  //       setTasks(todos)
  //     }
  //   } catch {
  //     alert('Couldn\'t restore previous session')
  //   }
  // }, [])
  //
  // useEffect(() => {
  //   localStorage.setItem('tasks', JSON.stringify(tasks))
  // }, [tasks])


  const addTask = event => {
    if (event.key === 'Enter' && taskTitle !== '') {
      clickBtn(event)
    }
  }
  const clickBtn = () => {
    if (taskTitle !== '') {
      setTasks([...tasks,
        {
          userId: tasks.length,
          id: Date.now(),
          title: taskTitle,
          completed: false
        }
      ])
      setTaskTitle('')
    }

  }

  return (
    <div className="App">
      <header className="App-header">
        <input type="text" value={taskTitle} onChange={event => setTaskTitle(event.target.value)}
               onKeyPress={addTask}/>
        <button className='add-button' onClick={clickBtn}><i className="fas fa-plus"></i></button>
      </header>
      <div className='container'>
        <h3>TodoList</h3>

        <select value={filter} onChange={event => setFilter(event.target.value)}>
          <option value='any'>Any</option>
          <option value='todo'>Todo</option>
          <option value='done'>Done</option>
        </select>


        <TaskList tasks={tasks} setTasks={setTasks} filter={filter}/>

      </div>
    </div>
  );
}

export default App;
