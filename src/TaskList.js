import React, {useMemo} from 'react'


export default function TaskList({tasks, setTasks, filter}){

  const checked = (completed, taskId) => {
    const newTasks = tasks.map((t) => t.id === taskId ? {...t, completed}: t )
    setTasks(newTasks)
  }

  const filtered = useMemo( () => {
    if (filter === 'done') {
      return tasks.filter((task) => task.completed === true)
    }
    if (filter === 'todo') {
      return tasks.filter((task) => task.completed === false)
    }
    return tasks

  }, [tasks,filter])

  const deleteTask = (taskId) => {
    // var index = tasks.map(task => {
    //   return task.id;
    // }).indexOf(taskId);

    const newTasks = tasks.filter((t) => t.id !== taskId)
    // newTasks.splice(index, 1);
    setTasks(newTasks)
  }

  if (tasks.length === 0) return <p>No tasks yet!!!</p>

  return (
    <table>
      <thead>
        <tr>
          <th>Done</th>
          <th>Task</th>
          <th>Remove</th>
        </tr>
      </thead>

      <tbody>
    {filtered.map((filteredTask) =>
      <tr key={filteredTask.id}
          className={filteredTask.completed ? 'completed' : ''}>
        <td>
          <input type="checkbox" checked={filteredTask.completed}
                   onChange={event => checked(event.target.checked, filteredTask.id)}/>
        </td>
        <td>
          <span>{filteredTask.title}</span>
        </td>
        <td>
          <button className='del-button' onClick={() => deleteTask(filteredTask.id)}><i className="fas fa-ban"></i> </button>
        </td>
        </tr>
    )}
      </tbody>
      </table>
  )
}
